/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PacUTN;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

/**
 *
 * @author Ricardo
 */
public class Cerveza {
   boolean comido;
   int x;
   int y;
   /**
    * metodo de pintar la fruta al ser comida la pone en true y luego se va a la 
    * 0 0  de la matriz o el mapa por asi decirlo desaparece 
    * @param g graficos que pintan la fruta 
    */
    public void pintarfruta(Graphics g,Pacman p, int puntos){
     if (comido == false && (p.puntosFruta>=puntos&&p.puntosFruta<(puntos*2))) {
            g.setColor(new Color(153,217,234));
            g.fillRect(x, y, 15, 30);
            g.setColor(Color.white);
            g.fillRect(x, y+5, 15, 2);
            g.setColor(Color.yellow);
            g.fillRect(x, y+7, 15, 20);
            
        } else {
            x = 0;
            y = 0;

        }
     
    }
    
    public Cerveza() {
    }

    public Cerveza(int x, int y) {
        this.x = x;
        this.y = y;
    }

    
    public boolean isComido() {
        return comido;
    }

    public void setComido(boolean comido) {
        this.comido = comido;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
    /**
     * son las intersecciones de las paredes 
     * @return un nuevo objeto de la importacion de rectangle
     */
     public Rectangle getBounds() {
        return new Rectangle(x, y, 5, 5);
    }
    
}
