/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PacUTN;

import javax.swing.JFrame;

/**
 *
 * @author Fabio_2
 */
public class Pacutn {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Tablero pacutn = new Tablero();
        JFrame frm = new JFrame();// Creamos un formulario
        frm.add(pacutn);
        frm.setSize(601, 450); // Le damos un tamaño a la ventana
        frm.setVisible(true); // Hace visible la ventana
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Accion por defecto al cerrar
        frm.setResizable(false);
        frm.setLocationRelativeTo(null); //Centrar la ventana
        frm.setTitle("PAC-UTN V.1.12.1 FabioValerio/RicardoRojas");
        while (true) {
            frm.repaint();
            try {
                Thread.sleep(pacutn.velocidad);
                

                pacutn.reset();
                if (pacutn.cerrar == true) {
                    frm.dispose();
                }
            } catch (InterruptedException ex) {

            }
        }

    }
}
