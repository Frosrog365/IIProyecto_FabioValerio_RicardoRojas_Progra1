/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PacUTN;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

/**
 *
 * @author Fabio_2
 */
public class Bolita {

    int x;
    int y;
    boolean comido;
    int score;

    public void setComido() {
        this.comido = false;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void pintarBolitas(Graphics g) {
        if (comido == false) {

            g.setColor(Color.CYAN);
            g.fillOval(x, y, 8, 8);
        } else {
            x = 0;
            y = 0;

        }
    }

    public Rectangle getBounds() {
        return new Rectangle(x, y, 5, 5);
    }
}
