/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PacUTN;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

/**
 *
 * @author Fabio_2
 */
public class BolitaMax {
    int x;
    int y;
    boolean comido;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public boolean isComido() {
        return comido;
    }

    public void setComido(boolean comido) {
        this.comido = comido;
    }

    public BolitaMax() {
    }
    public void pintarBolitas(Graphics g) {
        if (comido == false) {

            g.setColor(new Color(0, 128, 64));
            g.fillOval(x, y, 12, 12);
        } else {
            x = 0;
            y = 0;

        }
    }

    public Rectangle getBounds() {
        return new Rectangle(x, y, 5, 5);
}
}
