/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PacUTN;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

/**
 *
 * @author Ricardo
 */
public class Fantasmas {

    private boolean comido = false;
    private int x;
    private int y;
    private int veloX;
    private int veloY;
    private Color color;
    private int numero;

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    /**
     * pinta a el fantasma
     *
     * @param g Graficos como color y forma
     */
    public void pintar(Graphics g) {
        if (isComido()) {
            g.setColor(Color.BLUE);
        } else {
            g.setColor(color);
        }

        g.fillOval(x, y, 19, 20);
        g.setColor(Color.WHITE);
        g.fillOval(x+1,y+4,4,8);
        g.fillOval(x+8,y+4,4,8);
        g.setColor(Color.BLUE);
        g.fillOval(x+2,y+4,3,6);
        g.fillOval(x+9,y+4,3,6);

    }

    /**
     * Método para poder mover el fantasma
     *
     *
     */
    public void moverF() {
        switch (numero) {
            case 2:
                veloX = 0;
                veloY = 1;

                break;
            case 1:
                veloX = 0;
                veloY = -1;

                break;
            case 4:
                veloY = 0;
                veloX = 1;

                break;
            case 3:
                veloY = 0;
                veloX = -1;

                break;
        }
        x += veloX;
        y += veloY;
    }

    /**
     * Método para poder sacar los bordes de los fantasmas
     *
     * @return El rectangulo con bordes para poder intersectar los demás objetos
     */
    public Rectangle getBounds() {
        return new Rectangle(x, y, 20, 20);
    }

    /**
     * Método para que el fantasma choque contra las paredes
     *
     * @param pared La pared con la que va a chocar
     * @param PosX La posición X donde se situa el fantasma
     * @param PosY La posición Y donde se situa el fantasma
     */
    public void choque(Pared pared, int PosX, int PosY) {
        if (getBounds().intersects(pared.getBounds())) {
            x = PosX;
            y = PosY;
            veloX = 0;
            veloY = 0;
        }
    }

    /**
     * Funcion que permite que un fantasma pueda matar al pacman
     *
     * @param p El pacman para poder intersectar
     * @param t El tablero para obtener los puntos
     */
    public void matar(Pacman p,Tablero t) {
        if (getBounds().intersects(p.getBounds()) && isComido() == false) {
            p.setX(275);
            p.setY(230);
            p.setVida(p.getVida() - 1);
            x = 300;
            y = 150;
        } else if (getBounds().intersects(p.getBounds()) && isComido()) {
            x = 300;
            y = 150;
            t.fantasmasComidos++;
            comido = false;
            setColor(color);
        }
    }

    public Fantasmas() {
    }

    public Fantasmas(int x, int y, int veloX, int veloY) {
        this.x = x;
        this.y = y;
        this.veloX = veloX;
        this.veloY = veloY;
    }

    public boolean isComido() {
        return comido;
    }

    public void setComido(boolean comido) {
        this.comido = comido;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getVeloX() {
        return veloX;
    }

    public void setVeloX(int veloX) {
        this.veloX = veloX;
    }

    public int getVeloY() {
        return veloY;
    }

    public void setVeloY(int veloY) {
        this.veloY = veloY;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

}
