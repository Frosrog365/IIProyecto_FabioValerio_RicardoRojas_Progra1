/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PacUTN;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author Fabio_2
 */
public class Tablero extends JPanel implements KeyListener {

    Pared p1;
    Pared p2;
    Pared p3;
    Pared p4;
    Pared p5;
    Pared p6;
    Pared p7;
    Pared p8;
    Pared p9;
    Pared p10;
    Pared p11;
    Pared p12;
    Pared p13;
    Pared p14;
    Pared p15;
    Pared p16;
    Pared p17;
    Pared p18;
    Pared p19;
    Pared p20;
    Pared p21;
    Pared p22;
    Pared p23;

    Pacman p;
    Bolita b;
    Bolita b2;
    Bolita b3;
    Bolita b4;
    Bolita b5;
    Bolita b6;
    Bolita b7;
    Bolita b8;
    Bolita b9;
    Bolita b10;
    Bolita b11;
    Bolita b12;
    Bolita b13;
    Bolita b14;
    Bolita b15;
    Bolita b16;
    Bolita b17;
    Bolita b18;
    Bolita b19;
    Bolita b20;
    Bolita b21;
    Bolita b22;
    Bolita b23;
    Bolita b24;
    Bolita b25;
    Bolita b26;
    Bolita b27;
    Bolita b28;
    Bolita b29;
    Bolita b30;
    Bolita b31;
    Bolita b32;
    Bolita b33;
    Bolita b34;
    Bolita b35;
    Bolita b36;
    Bolita b37;
    Bolita b38;
    Bolita b39;
    Bolita b40;
    Bolita b41;
    Bolita b42;
    Bolita b43;
    Bolita b44;
    Bolita b45;
    Bolita b46;
    Bolita b47;
    Bolita b48;
    Bolita b49;
    Bolita b50;
    Bolita b51;
    Bolita b52;
    Bolita b53;
    Bolita b54;
    Bolita b55;
    Bolita b56;
    Bolita b57;
    Bolita b58;
    Bolita b59;
    Bolita b60;
    Bolita b61;
    Bolita b62;
    Bolita b63;
    Bolita b64;
    Bolita b65;
    Bolita b66;
    Bolita b67;
    Bolita b68;
    Bolita b69;
    Bolita b70;
    Bolita b71;
    Bolita b72;
    Bolita b73;
    Bolita b74;
    Bolita b75;
    Bolita b76;
    Bolita b77;
    Bolita b78;
    Bolita b79;
    Bolita b80;
    Bolita b81;
    Bolita b82;
    Bolita b83;
    Bolita b84;
    Bolita b85;
    Bolita b86;
    Bolita b87;
    Bolita b88;
    Bolita b89;
    Bolita b90;
    Bolita b91;
    Bolita b92;
    Bolita b93;
    Bolita b94;
    Bolita b95;
    Bolita b96;
    Bolita b97;
    Bolita b98;
    Bolita b99;
    Bolita b100;
    Bolita b101;
    Bolita b102;
    Bolita b103;
    Bolita b104;
    Bolita b105;

    BolitaMax bm1;
    BolitaMax bm2;
    BolitaMax bm3;
    BolitaMax bm4;

    Fantasmas fa1;
    Fantasmas fa2;
    Fantasmas fa3;
    Fantasmas fa4;

    Fruta fru;
    Cerveza fru2;

    int score;
    boolean pausa;
    boolean empezar;
    boolean cerrar;
    int random;
    int camino;
    int random2;
    int random3;
    int random4;
    int random5;
    int nivel;
    int velocidad;
    int cont;
    int random6;
    int fantasmasComidos;
    String nombre;
    String list;

    public Tablero() {
        setFocusable(true);
        addKeyListener(this);
        this.nivel = 1;
        this.fantasmasComidos = 0;
        this.velocidad = 10;
        this.cont = 0;
        this.camino = 0;
        this.p1 = new Pared();
        this.p2 = new Pared();
        this.p3 = new Pared();
        this.p4 = new Pared();
        this.p5 = new Pared();
        this.p6 = new Pared();
        this.p7 = new Pared();
        this.p8 = new Pared();
        this.p9 = new Pared();
        this.p10 = new Pared();
        this.p11 = new Pared();
        this.p12 = new Pared();
        this.p13 = new Pared();
        this.p14 = new Pared();
        this.p15 = new Pared();
        this.p16 = new Pared();
        this.p17 = new Pared();
        this.p18 = new Pared();
        this.p19 = new Pared();
        this.p20 = new Pared();
        this.p21 = new Pared();
        this.p22 = new Pared();
        this.p23 = new Pared();

        this.p = new Pacman();
        this.fa1 = new Fantasmas();
        this.fa2 = new Fantasmas();
        this.fa3 = new Fantasmas();
        this.fa4 = new Fantasmas();
        this.fru = new Fruta();
        this.fru2 = new Cerveza();
        this.b = new Bolita();
        this.b2 = new Bolita();
        this.b3 = new Bolita();
        this.b4 = new Bolita();
        this.b5 = new Bolita();
        this.b6 = new Bolita();
        this.b7 = new Bolita();
        this.b8 = new Bolita();
        this.b9 = new Bolita();
        this.b10 = new Bolita();
        this.b11 = new Bolita();
        this.b12 = new Bolita();
        this.b13 = new Bolita();
        this.b14 = new Bolita();
        this.b15 = new Bolita();
        this.b16 = new Bolita();
        this.b17 = new Bolita();
        this.b18 = new Bolita();
        this.b19 = new Bolita();
        this.b20 = new Bolita();
        this.b21 = new Bolita();
        this.b22 = new Bolita();
        this.b23 = new Bolita();
        this.b24 = new Bolita();
        this.b25 = new Bolita();
        this.b26 = new Bolita();
        this.b27 = new Bolita();
        this.b28 = new Bolita();
        this.b29 = new Bolita();
        this.b30 = new Bolita();
        this.b31 = new Bolita();
        this.b32 = new Bolita();
        this.b33 = new Bolita();
        this.b34 = new Bolita();
        this.b35 = new Bolita();
        this.b36 = new Bolita();
        this.b37 = new Bolita();
        this.b38 = new Bolita();
        this.b39 = new Bolita();
        this.b40 = new Bolita();
        this.b41 = new Bolita();
        this.b42 = new Bolita();
        this.b43 = new Bolita();
        this.b44 = new Bolita();
        this.b45 = new Bolita();
        this.b46 = new Bolita();
        this.b47 = new Bolita();
        this.b48 = new Bolita();
        this.b49 = new Bolita();
        this.b50 = new Bolita();
        this.b51 = new Bolita();
        this.b52 = new Bolita();
        this.b53 = new Bolita();
        this.b54 = new Bolita();
        this.b55 = new Bolita();
        this.b56 = new Bolita();
        this.b57 = new Bolita();
        this.b58 = new Bolita();
        this.b59 = new Bolita();
        this.b60 = new Bolita();
        this.b61 = new Bolita();
        this.b62 = new Bolita();
        this.b63 = new Bolita();
        this.b64 = new Bolita();
        this.b65 = new Bolita();
        this.b66 = new Bolita();
        this.b67 = new Bolita();
        this.b68 = new Bolita();
        this.b69 = new Bolita();
        this.b70 = new Bolita();
        this.b71 = new Bolita();
        this.b72 = new Bolita();
        this.b73 = new Bolita();
        this.b74 = new Bolita();
        this.b75 = new Bolita();
        this.b76 = new Bolita();
        this.b77 = new Bolita();
        this.b78 = new Bolita();
        this.b79 = new Bolita();
        this.b80 = new Bolita();
        this.b81 = new Bolita();
        this.b82 = new Bolita();
        this.b83 = new Bolita();
        this.b84 = new Bolita();
        this.b85 = new Bolita();
        this.b86 = new Bolita();
        this.b87 = new Bolita();
        this.b88 = new Bolita();
        this.b89 = new Bolita();
        this.b90 = new Bolita();
        this.b91 = new Bolita();
        this.b92 = new Bolita();
        this.b93 = new Bolita();
        this.b94 = new Bolita();
        this.b95 = new Bolita();
        this.b96 = new Bolita();
        this.b97 = new Bolita();
        this.b98 = new Bolita();
        this.b99 = new Bolita();
        this.b100 = new Bolita();
        this.b101 = new Bolita();
        this.b102 = new Bolita();
        this.b103 = new Bolita();
        this.b104 = new Bolita();
        this.b105 = new Bolita();

        this.bm1 = new BolitaMax();
        this.bm2 = new BolitaMax();
        this.bm3 = new BolitaMax();
        this.bm4 = new BolitaMax();
        this.score = 0;
        p.setX(275);
        p.setY(230);
        p.setVida(3);
        fa1.setX(300);
        fa1.setY(150);
        fa2.setX(275);
        fa2.setY(150);
        fa3.setX(300);
        fa3.setY(175);
        fa4.setX(275);
        fa4.setY(175);

        this.pausa = false;
        this.empezar = false;
        this.cerrar = false;

        this.list = "Fabio        1800\n"
                + "Elena         500\n"
                + "Ricardo      2700\n"
                + "Valeria      1000\n";

    }

    public void subirNivel() {
        p.puntosFruta = 0;
        b.comido = false;
        b2.comido = false;
        fru.comido = false;
        fru2.comido = false;
        b3.comido = false;
        b4.comido = false;
        b5.comido = false;
        b6.comido = false;
        b7.comido = false;
        b8.comido = false;
        b9.comido = false;
        b10.comido = false;
        b11.comido = false;
        b12.comido = false;
        b13.comido = false;
        b14.comido = false;
        b15.comido = false;
        b16.comido = false;
        b17.comido = false;
        b18.comido = false;
        b19.comido = false;
        b20.comido = false;
        b21.comido = false;
        b22.comido = false;
        b23.comido = false;
        b24.comido = false;
        b25.comido = false;
        b26.comido = false;
        b27.comido = false;
        b28.comido = false;
        b29.comido = false;
        b30.comido = false;
        b31.comido = false;
        b32.comido = false;
        b33.comido = false;
        b34.comido = false;
        b35.comido = false;
        b36.comido = false;
        b37.comido = false;
        b38.comido = false;
        b39.comido = false;
        b40.comido = false;
        b41.comido = false;
        b42.comido = false;
        b43.comido = false;
        b44.comido = false;
        b45.comido = false;
        b46.comido = false;
        b47.comido = false;
        b48.comido = false;
        b49.comido = false;
        b50.comido = false;
        b51.comido = false;
        b52.comido = false;
        b53.comido = false;
        b54.comido = false;
        b55.comido = false;
        b56.comido = false;
        b57.comido = false;
        b58.comido = false;
        b59.comido = false;
        b60.comido = false;
        b61.comido = false;
        b62.comido = false;
        b63.comido = false;
        b64.comido = false;
        b65.comido = false;
        b66.comido = false;
        b67.comido = false;
        b68.comido = false;
        b69.comido = false;
        b70.comido = false;
        b71.comido = false;
        b72.comido = false;
        b73.comido = false;
        b74.comido = false;
        b75.comido = false;
        b76.comido = false;
        b77.comido = false;
        b78.comido = false;
        b79.comido = false;
        b80.comido = false;
        b81.comido = false;
        b82.comido = false;
        b83.comido = false;
        b84.comido = false;
        b85.comido = false;
        b86.comido = false;
        b87.comido = false;
        b88.comido = false;
        b89.comido = false;
        b90.comido = false;
        b91.comido = false;
        b92.comido = false;
        b93.comido = false;
        b94.comido = false;
        b95.comido = false;
        b96.comido = false;
        b97.comido = false;
        b98.comido = false;
        b99.comido = false;
        b100.comido = false;
        b101.comido = false;
        b102.comido = false;
        b103.comido = false;
        b104.comido = false;
        b105.comido = false;
        bm1.comido = false;
        bm2.comido = false;
        bm3.comido = false;
        bm4.comido = false;
        p.setX(275);
        p.setY(230);
        fa1.setX(300);
        fa1.setY(150);
        fa2.setX(275);
        fa2.setY(150);
        fa3.setX(300);
        fa3.setY(175);
        fa4.setX(275);
        fa4.setY(175);
        fa1.setComido(false);
        fa2.setComido(false);
        fa3.setComido(false);
        fa4.setComido(false);
        empezar = false;

        if (nivel == 1) {
            velocidad = 10;
        } else if (nivel == 2) {
            velocidad = 8;
        } else if (nivel == 3) {
            velocidad = 6;
        }
    }

    /**
     * Método para resetear el tablero
     */
    public void reset() {
        if (p.getVida() == -1) {
            p.puntosFruta = 0;
            nivel = 1;
            p.puntosvida = 0;
            p.bolitasComidas = 0;
            nombre = JOptionPane.showInputDialog("Digite su nombre");
            list += nombre + "           ";
            list += Integer.toString(p.getScore()) + "\n";
            empezar = false;
            fru.comido = false;
            fru2.comido = false;
            p.setVida(3);
            b.comido = false;
            b2.comido = false;
            b3.comido = false;
            b4.comido = false;
            b5.comido = false;
            b6.comido = false;
            b7.comido = false;
            b8.comido = false;
            b9.comido = false;
            b10.comido = false;
            b11.comido = false;
            b12.comido = false;
            b13.comido = false;
            b14.comido = false;
            b15.comido = false;
            b16.comido = false;
            b17.comido = false;
            b18.comido = false;
            b19.comido = false;
            b20.comido = false;
            b21.comido = false;
            b22.comido = false;
            b23.comido = false;
            b24.comido = false;
            b25.comido = false;
            b26.comido = false;
            b27.comido = false;
            b28.comido = false;
            b29.comido = false;
            b30.comido = false;
            b31.comido = false;
            b32.comido = false;
            b33.comido = false;
            b34.comido = false;
            b35.comido = false;
            b36.comido = false;
            b37.comido = false;
            b38.comido = false;
            b39.comido = false;
            b40.comido = false;
            b41.comido = false;
            b42.comido = false;
            b43.comido = false;
            b44.comido = false;
            b45.comido = false;
            b46.comido = false;
            b47.comido = false;
            b48.comido = false;
            b49.comido = false;
            b50.comido = false;
            b51.comido = false;
            b52.comido = false;
            b53.comido = false;
            b54.comido = false;
            b55.comido = false;
            b56.comido = false;
            b57.comido = false;
            b58.comido = false;
            b59.comido = false;
            b60.comido = false;
            b61.comido = false;
            b62.comido = false;
            b63.comido = false;
            b64.comido = false;
            b65.comido = false;
            b66.comido = false;
            b67.comido = false;
            b68.comido = false;
            b69.comido = false;
            b70.comido = false;
            b71.comido = false;
            b72.comido = false;
            b73.comido = false;
            b74.comido = false;
            b75.comido = false;
            b76.comido = false;
            b77.comido = false;
            b78.comido = false;
            b79.comido = false;
            b80.comido = false;
            b81.comido = false;
            b82.comido = false;
            b83.comido = false;
            b84.comido = false;
            b85.comido = false;
            b86.comido = false;
            b87.comido = false;
            b88.comido = false;
            b89.comido = false;
            b90.comido = false;
            b91.comido = false;
            b92.comido = false;
            b93.comido = false;
            b94.comido = false;
            b95.comido = false;
            b96.comido = false;
            b97.comido = false;
            b98.comido = false;
            b99.comido = false;
            b100.comido = false;
            b101.comido = false;
            b102.comido = false;
            b103.comido = false;
            b104.comido = false;
            b105.comido = false;
            bm1.comido = false;
            bm2.comido = false;
            bm3.comido = false;
            bm4.comido = false;
            p.setX(275);
            p.setY(230);
            fa1.setComido(false);
            fa2.setComido(false);
            fa3.setComido(false);
            fa4.setComido(false);
            fa1.setX(300);
            fa1.setY(150);
            fa2.setX(275);
            fa2.setY(150);
            fa3.setX(300);
            fa3.setY(175);
            fa4.setX(275);
            fa4.setY(175);
            p.setScore(0);

        }
    }

    /**
     * Método para resetear por medio de la tecla
     */
    public void resetTecla() {
        p.setVida(3);
        p.puntosFruta = 0;
        fru.comido = false;
        fru2.comido = false;
        p.puntosvida = 0;
        p.bolitasComidas = 0;
        b.comido = false;
        b2.comido = false;
        b3.comido = false;
        b4.comido = false;
        b5.comido = false;
        b6.comido = false;
        b7.comido = false;
        b8.comido = false;
        b9.comido = false;
        b10.comido = false;
        b11.comido = false;
        b12.comido = false;
        b13.comido = false;
        b14.comido = false;
        b15.comido = false;
        b16.comido = false;
        b17.comido = false;
        b18.comido = false;
        b19.comido = false;
        b20.comido = false;
        b21.comido = false;
        b22.comido = false;
        b23.comido = false;
        b24.comido = false;
        b25.comido = false;
        b26.comido = false;
        b27.comido = false;
        b28.comido = false;
        b29.comido = false;
        b30.comido = false;
        b31.comido = false;
        b32.comido = false;
        b33.comido = false;
        b34.comido = false;
        b35.comido = false;
        b36.comido = false;
        b37.comido = false;
        b38.comido = false;
        b39.comido = false;
        b40.comido = false;
        b41.comido = false;
        b42.comido = false;
        b43.comido = false;
        b44.comido = false;
        b45.comido = false;
        b46.comido = false;
        b47.comido = false;
        b48.comido = false;
        b49.comido = false;
        b50.comido = false;
        b51.comido = false;
        b52.comido = false;
        b53.comido = false;
        b54.comido = false;
        b55.comido = false;
        b56.comido = false;
        b57.comido = false;
        b58.comido = false;
        b59.comido = false;
        b60.comido = false;
        b61.comido = false;
        b62.comido = false;
        b63.comido = false;
        b64.comido = false;
        b65.comido = false;
        b66.comido = false;
        b67.comido = false;
        b68.comido = false;
        b69.comido = false;
        b70.comido = false;
        b71.comido = false;
        b72.comido = false;
        b73.comido = false;
        b74.comido = false;
        b75.comido = false;
        b76.comido = false;
        b77.comido = false;
        b78.comido = false;
        b79.comido = false;
        b80.comido = false;
        b81.comido = false;
        b82.comido = false;
        b83.comido = false;
        b84.comido = false;
        b85.comido = false;
        b86.comido = false;
        b87.comido = false;
        b88.comido = false;
        b89.comido = false;
        b90.comido = false;
        b91.comido = false;
        b92.comido = false;
        b93.comido = false;
        b94.comido = false;
        b95.comido = false;
        b96.comido = false;
        b97.comido = false;
        b98.comido = false;
        b99.comido = false;
        b100.comido = false;
        b101.comido = false;
        b102.comido = false;
        b103.comido = false;
        b104.comido = false;
        b105.comido = false;
        bm1.comido = false;
        bm2.comido = false;
        bm3.comido = false;
        bm4.comido = false;
        p.setScore(0);
        p.setX(275);
        p.setY(230);
        fa1.setX(300);
        fa1.setY(150);
        fa2.setX(275);
        fa2.setY(150);
        fa3.setX(300);
        fa3.setY(175);
        fa4.setX(275);
        fa4.setY(175);
        fa1.setComido(false);
        fa2.setComido(false);
        fa3.setComido(false);
        fa4.setComido(false);
        empezar = false;
    }

    /**
     * metodo para pintar los fantasmas pasar nivel aumentar score y pintar
     * pacman obtener colisiones. pintar las bolitas o comida del pacman y la
     * bolade poder
     *
     * @param g graphico en el cual se pinta en el panel los objetos bolitas
     * bolitasmax y la fruta pasa de nivel o aumenta la velocidad
     */
    @Override
    public void paint(Graphics g
    ) {
        super.paint(g);
//        this.random = (int) (Math.random() * 4) + 1;
        p1.setColor(Color.GREEN);
        fa1.setColor(Color.CYAN);
        fa2.setColor(new Color(255, 128, 0));
        fa3.setColor(Color.PINK);
        fa4.setColor(Color.RED);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setColor(Color.BLACK);
        g2d.fillRect(0, 0, getWidth(), getHeight());
        g2d.setColor(Color.yellow);
        g2d.setFont(new Font("Arial", Font.PLAIN, 20));
        g2d.drawString("Puntuación: " + p.getScore(), 40, 400);
        g2d.drawString("Nivel: " + nivel, 250, 400);
        g2d.drawString("Vidas: " + p.getVida(), 400, 400);
        p.ganarVida();
        if (p.bolitasComidas < 108) {
            switch (nivel) {
                case 1:
                    velocidad = 10;
                    break;
                case 2:
                    velocidad = 8;
                    break;
                case 3:
                    velocidad = 6;
                    break;
                default:
                    break;
            }
        } else if (p.bolitasComidas == 109) {
            nivel++;
            subirNivel();
            p.bolitasComidas = 0;
        }

        if (fa1.isComido() || fa2.isComido() || fa3.isComido() || fa4.isComido()) {
            cont++;
            if (cont == 1000) {
                fa1.setComido(false);
                fa2.setComido(false);
                fa3.setComido(false);
                fa4.setComido(false);
                cont = 0;
            }
        }
        if (fa1.isComido() || fa2.isComido() || fa3.isComido() || fa4.isComido()) {
            if (fantasmasComidos == 1) {
                p.setScore(p.getScore() + 200);
                p.puntosvida += 200;
                p.puntosFruta += 200;
                fantasmasComidos = 2;
            } else if (fantasmasComidos == 3) {
                p.setScore(p.getScore() + 400);
                p.puntosvida += 400;
                p.puntosFruta += 400;
                fantasmasComidos = 4;
            } else if (fantasmasComidos == 5) {
                p.setScore(p.getScore() + 800);
                p.puntosvida += 800;
                p.puntosFruta += 800;
                fantasmasComidos = 6;
            } else if (fantasmasComidos == 7) {
                p.setScore(p.getScore() + 1600);
                p.puntosFruta += 1600;
                p.puntosvida += 1600;
                fantasmasComidos = 0;
            }
        } else {
            fantasmasComidos = 0;
        }

        camino++;
        if (camino == 50) {
            random = (int) (Math.random() * 4) + 1;
            random2 = (int) (Math.random() * 4) + 1;
            random3 = (int) (Math.random() * 4) + 1;
            random4 = (int) (Math.random() * 4) + 1;
            camino = 0;
        }

        b.setX(30);
        b.setY(70);
        b.pintarBolitas(g2d);
        p.comer(b, -1);

        b2.setX(30);
        b2.setY(110);
        b2.pintarBolitas(g2d);
        p.comer(b2, -1);

        b3.setX(30);
        b3.setY(150);
        b3.pintarBolitas(g2d);
        p.comer(b3, -1);

        b4.setX(30);
        b4.setY(200);
        b4.pintarBolitas(g2d);
        p.comer(b4, -1);

        b5.setX(30);
        b5.setY(240);
        b5.pintarBolitas(g2d);
        p.comer(b5, -1);

        b6.setX(30);
        b6.setY(280);
        b6.pintarBolitas(g2d);
        p.comer(b6, -1);

        b7.setX(30);
        b7.setY(320);
        b7.pintarBolitas(g2d);
        p.comer(b7, -1);

        b8.setX(70);
        b8.setY(70);
        b8.pintarBolitas(g2d);
        p.comer(b8, -1);

        b9.setX(70);
        b9.setY(110);
        b9.pintarBolitas(g2d);
        p.comer(b9, -1);

        b10.setX(70);
        b10.setY(150);
        b10.pintarBolitas(g2d);
        p.comer(b10, -1);

        b11.setX(70);
        b11.setY(200);
        b11.pintarBolitas(g2d);
        p.comer(b11, -1);

        b12.setX(70);
        b12.setY(240);
        b12.pintarBolitas(g2d);
        p.comer(b12, -1);

        b13.setX(70);
        b13.setY(280);
        b13.pintarBolitas(g2d);
        p.comer(b13, -1);

        b14.setX(70);
        b14.setY(30);
        b14.pintarBolitas(g2d);
        p.comer(b14, -1);

        b15.setX(110);
        b15.setY(30);
        b15.pintarBolitas(g2d);
        p.comer(b15, -1);

        b16.setX(150);
        b16.setY(30);
        b16.pintarBolitas(g2d);
        p.comer(b16, -1);

        b17.setX(110);
        b17.setY(70);
        b17.pintarBolitas(g2d);
        p.comer(b17, -1);

        b18.setX(150);
        b18.setY(70);
        b18.pintarBolitas(g2d);
        p.comer(b18, -1);

        b19.setX(110);
        b19.setY(300);
        b19.pintarBolitas(g2d);
        p.comer(b19, -1);

        b20.setX(150);
        b20.setY(300);
        b20.pintarBolitas(g2d);
        p.comer(b20, -1);

        b21.setX(190);
        b21.setY(340);
        b21.pintarBolitas(g2d);
        p.comer(b21, -1);

        b22.setX(230);
        b22.setY(340);
        b22.pintarBolitas(g2d);
        p.comer(b22, -1);

        b23.setX(270);
        b23.setY(340);
        b23.pintarBolitas(g2d);
        p.comer(b23, -1);

        b24.setX(310);
        b24.setY(340);
        b24.pintarBolitas(g2d);
        p.comer(b24, -1);

        b25.setX(350);
        b25.setY(340);
        b25.pintarBolitas(g2d);
        p.comer(b25, -1);

        b26.setX(390);
        b26.setY(340);
        b26.pintarBolitas(g2d);
        p.comer(b26, -1);

        b27.setX(430);
        b27.setY(340);
        b27.pintarBolitas(g2d);
        p.comer(b27, -1);

        b28.setX(470);
        b28.setY(340);
        b28.pintarBolitas(g2d);
        p.comer(b28, -1);

        b29.setX(470);
        b29.setY(70);
        b29.pintarBolitas(g2d);
        p.comer(b29, -1);

        b30.setX(510);
        b30.setY(280);
        b30.pintarBolitas(g2d);
        p.comer(b30, -1);

        b31.setX(510);
        b31.setY(240);
        b31.pintarBolitas(g2d);
        p.comer(b31, -1);

        b32.setX(510);
        b32.setY(200);
        b32.pintarBolitas(g2d);
        p.comer(b32, -1);

        b33.setX(510);
        b33.setY(150);
        b33.pintarBolitas(g2d);
        p.comer(b33, -1);

        b34.setX(510);
        b34.setY(110);
        b34.pintarBolitas(g2d);
        p.comer(b34, -1);

        b35.setX(510);
        b35.setY(70);
        b35.pintarBolitas(g2d);
        p.comer(b35, -1);

        b36.setX(430);
        b36.setY(70);
        b36.pintarBolitas(g2d);
        p.comer(b36, -1);

        b37.setX(430);
        b37.setY(30);
        b37.pintarBolitas(g2d);
        p.comer(b37, -1);

        b38.setX(470);
        b38.setY(30);
        b38.pintarBolitas(g2d);
        p.comer(b38, -1);

        b39.setX(510);
        b39.setY(30);
        b39.pintarBolitas(g2d);
        p.comer(b39, -1);

        b40.setX(550);
        b40.setY(70);
        b40.pintarBolitas(g2d);
        p.comer(b40, -1);

        b41.setX(550);
        b41.setY(110);
        b41.pintarBolitas(g2d);
        p.comer(b41, -1);

        b42.setX(550);
        b42.setY(150);
        b42.pintarBolitas(g2d);
        p.comer(b42, -1);

        b43.setX(550);
        b43.setY(200);
        b43.pintarBolitas(g2d);
        p.comer(b43, -1);

        b44.setX(550);
        b44.setY(240);
        b44.pintarBolitas(g2d);
        p.comer(b44, -1);

        b45.setX(550);
        b45.setY(280);
        b45.pintarBolitas(g2d);
        p.comer(b45, -1);

        b46.setX(550);
        b46.setY(320);
        b46.pintarBolitas(g2d);
        p.comer(b46, -1);

        b47.setX(470);
        b47.setY(280);
        b47.pintarBolitas(g2d);
        p.comer(b47, -1);

        b48.setX(470);
        b48.setY(240);
        b48.pintarBolitas(g2d);
        p.comer(b48, -1);

        b49.setX(470);
        b49.setY(200);
        b49.pintarBolitas(g2d);
        p.comer(b49, -1);

        b50.setX(470);
        b50.setY(150);
        b50.pintarBolitas(g2d);
        p.comer(b50, -1);

        b51.setX(470);
        b51.setY(110);
        b51.pintarBolitas(g2d);
        p.comer(b51, -1);

        b52.setX(420);
        b52.setY(110);
        b52.pintarBolitas(g2d);
        p.comer(b52, -1);

        b53.setX(420);
        b53.setY(150);
        b53.pintarBolitas(g2d);
        p.comer(b53, -1);

        b54.setX(420);
        b54.setY(200);
        b54.pintarBolitas(g2d);
        p.comer(b54, -1);

        b55.setX(420);
        b55.setY(240);
        b55.pintarBolitas(g2d);
        p.comer(b55, -1);

        b56.setX(380);
        b56.setY(240);
        b56.pintarBolitas(g2d);
        p.comer(b56, -1);

        b57.setX(380);
        b57.setY(200);
        b57.pintarBolitas(g2d);
        p.comer(b57, -1);

        b58.setX(380);
        b58.setY(150);
        b58.pintarBolitas(g2d);
        p.comer(b58, -1);

        b59.setX(380);
        b59.setY(110);
        b59.pintarBolitas(g2d);
        p.comer(b59, -1);

        b60.setX(380);
        b60.setY(70);
        b60.pintarBolitas(g2d);
        p.comer(b60, -1);

        b61.setX(380);
        b61.setY(30);
        b61.pintarBolitas(g2d);
        p.comer(b61, -1);

        b62.setX(340);
        b62.setY(30);
        b62.pintarBolitas(g2d);
        p.comer(b62, -1);

        b63.setX(300);
        b63.setY(30);
        b63.pintarBolitas(g2d);
        p.comer(b63, -1);

        b64.setX(260);
        b64.setY(30);
        b64.pintarBolitas(g2d);
        p.comer(b64, -1);

        b65.setX(220);
        b65.setY(30);
        b65.pintarBolitas(g2d);
        p.comer(b65, -1);

        b66.setX(190);
        b66.setY(30);
        b66.pintarBolitas(g2d);
        p.comer(b66, -1);

        b67.setX(190);
        b67.setY(70);
        b67.pintarBolitas(g2d);
        p.comer(b67, -1);

        b68.setX(190);
        b68.setY(110);
        b68.pintarBolitas(g2d);
        p.comer(b68, -1);

        b69.setX(190);
        b69.setY(150);
        b69.pintarBolitas(g2d);
        p.comer(b69, -1);

        b70.setX(190);
        b70.setY(200);
        b70.pintarBolitas(g2d);
        p.comer(b70, -1);

        b71.setX(190);
        b71.setY(240);
        b71.pintarBolitas(g2d);
        p.comer(b71, -1);

        b72.setX(110);
        b72.setY(110);
        b72.pintarBolitas(g2d);
        p.comer(b72, -1);

        b73.setX(110);
        b73.setY(150);
        b73.pintarBolitas(g2d);
        p.comer(b73, -1);

        b74.setX(110);
        b74.setY(200);
        b74.pintarBolitas(g2d);
        p.comer(b74, -1);

        b75.setX(110);
        b75.setY(240);
        b75.pintarBolitas(g2d);
        p.comer(b75, -1);

        b76.setX(110);
        b76.setY(280);
        b76.pintarBolitas(g2d);
        p.comer(b76, -1);

        b105.setX(150);
        b105.setY(110);
        b105.pintarBolitas(g2d);
        p.comer(b105, -1);

        b77.setX(150);
        b77.setY(150);
        b77.pintarBolitas(g2d);
        p.comer(b77, -1);

        b78.setX(150);
        b78.setY(200);
        b78.pintarBolitas(g2d);
        p.comer(b78, -1);

        b79.setX(150);
        b79.setY(240);
        b79.pintarBolitas(g2d);
        p.comer(b79, -1);

        b80.setX(230);
        b80.setY(240);
        b80.pintarBolitas(g2d);
        p.comer(b80, -1);

        b81.setX(310);
        b81.setY(240);
        b81.pintarBolitas(g2d);
        p.comer(b81, -1);

        b82.setX(350);
        b82.setY(240);
        b82.pintarBolitas(g2d);
        p.comer(b82, -1);

        b83.setX(70);
        b83.setY(340);
        b83.pintarBolitas(g2d);
        p.comer(b83, -1);

        b84.setX(110);
        b84.setY(340);
        b84.pintarBolitas(g2d);
        p.comer(b84, -1);

        b85.setX(150);
        b85.setY(340);
        b85.pintarBolitas(g2d);
        p.comer(b85, -1);

        b86.setX(430);
        b86.setY(340);
        b86.pintarBolitas(g2d);
        p.comer(b86, -1);

        b87.setX(470);
        b87.setY(340);
        b87.pintarBolitas(g2d);
        p.comer(b87, -1);

        b88.setX(510);
        b88.setY(340);
        b88.pintarBolitas(g2d);
        p.comer(b88, -1);

        b89.setX(430);
        b89.setY(300);
        b89.pintarBolitas(g2d);
        p.comer(b89, -1);

        b90.setX(470);
        b90.setY(300);
        b90.pintarBolitas(g2d);
        p.comer(b90, -1);

        b91.setX(390);
        b91.setY(300);
        b91.pintarBolitas(g2d);
        p.comer(b91, -1);

        b92.setX(350);
        b92.setY(300);
        b92.pintarBolitas(g2d);
        p.comer(b92, -1);

        b93.setX(310);
        b93.setY(300);
        b93.pintarBolitas(g2d);
        p.comer(b93, -1);

        b94.setX(270);
        b94.setY(300);
        b94.pintarBolitas(g2d);
        p.comer(b94, -1);

        b95.setX(230);
        b95.setY(300);
        b95.pintarBolitas(g2d);
        p.comer(b95, -1);

        b96.setX(190);
        b96.setY(300);
        b96.pintarBolitas(g2d);
        p.comer(b96, -1);

        b97.setX(230);
        b97.setY(110);
        b97.pintarBolitas(g2d);
        p.comer(b97, -1);

        b98.setX(270);
        b98.setY(110);
        b98.pintarBolitas(g2d);
        p.comer(b98, -1);

        b99.setX(310);
        b99.setY(110);
        b99.pintarBolitas(g2d);
        p.comer(b99, -1);

        b100.setX(350);
        b100.setY(110);
        b100.pintarBolitas(g2d);
        p.comer(b100, -1);

        b101.setX(350);
        b101.setY(70);
        b101.pintarBolitas(g2d);
        p.comer(b101, -1);

        b102.setX(310);
        b102.setY(70);
        b102.pintarBolitas(g2d);
        p.comer(b102, -1);

        b103.setX(270);
        b103.setY(70);
        b103.pintarBolitas(g2d);
        p.comer(b103, -1);

        b104.setX(230);
        b104.setY(70);
        b104.pintarBolitas(g2d);
        p.comer(b104, -1);

        bm1.setX(550);
        bm1.setY(30);
        bm1.pintarBolitas(g2d);
        p.comerBolitaMax(bm1, fa1, fa2, fa3, fa4, -1);

        bm2.setX(30);
        bm2.setY(30);
        bm2.pintarBolitas(g2d);
        p.comerBolitaMax(bm2, fa1, fa2, fa3, fa4, -1);

        bm3.setX(30);
        bm3.setY(330);
        bm3.pintarBolitas(g2d);
        p.comerBolitaMax(bm3, fa1, fa2, fa3, fa4, -1);

        bm4.setX(550);
        bm4.setY(330);
        bm4.pintarBolitas(g2d);
        p.comerBolitaMax(bm4, fa1, fa2, fa3, fa4, -1);

        //hacer el aleatorio
        fru.setX(65);
        fru.setY(300);
        fru.pintarfruta(g2d, p, 400);
        p.comerFruta(fru, -1);
        System.out.println(p.puntosFruta);

        fru2.setX(510);
        fru2.setY(300);
        fru2.pintarfruta(g2d, p, 800);
        p.comerCerveza(fru2, -1);

        p1.setX(10);
        p1.setY(10);
        p1.setAncho(10);
        p1.setLargo(350);
        p1.pintarPared(g2d);
        p.choque(p1, 20, p.getY());
        fa4.choque(p1, 20, fa4.getY());
        fa1.choque(p1, 20, fa1.getY());
        fa2.choque(p1, 20, fa2.getY());
        fa3.choque(p1, 20, fa3.getY());

        p2.setX(10);
        p2.setY(10);
        p2.setAncho(getWidth() - 20);
        p2.setLargo(10);
        p2.pintarPared(g2d);
        p.choque(p2, p.getX(), 20);
        fa4.choque(p2, fa4.getX(), 20);
        fa1.choque(p2, fa1.getX(), 20);
        fa2.choque(p2, fa2.getX(), 20);
        fa3.choque(p2, fa3.getX(), 20);

        p1.setX(10);
        p1.setY(350);
        p1.setAncho(getWidth() - 20);
        p1.setLargo(10);
        p1.pintarPared(g2d);
        p.choque(p1, p.getX(), 330);
        fa4.choque(p1, fa4.getX(), 330);
        fa3.choque(p1, fa3.getX(), 330);
        fa2.choque(p1, fa2.getX(), 330);
        fa1.choque(p1, fa1.getX(), 330);

        p1.setX(getWidth() - 20);
        p1.setY(10);
        p1.setAncho(10);
        p1.setLargo(350);
        p1.pintarPared(g2d);
        p.choque(p1, 555, p.getY());
        fa4.choque(p1, 555, fa4.getY());
        fa3.choque(p1, 555, fa3.getY());
        fa2.choque(p1, 555, fa2.getY());
        fa1.choque(p1, 555, fa1.getY());

        p1.setX(45);
        p1.setY(45);
        p1.setAncho(5);
        p1.setLargo(100);
        p1.pintarPared(g2d);
        p.choque(p1, p.getX() - 1, p.getY());
        fa4.choque(p1, fa4.getX() - 1, fa4.getY());
        fa3.choque(p1, fa3.getX() - 1, fa3.getY());
        fa2.choque(p1, fa2.getX() - 1, fa2.getY());
        fa1.choque(p1, fa1.getX() - 1, fa1.getY());

        p2.setX(50);
        p2.setY(45);
        p2.setAncho(5);
        p2.setLargo(100);
        p2.pintarPared(g2d);
        p.choque(p2, p.getX() + 1, p.getY());
        fa4.choque(p2, fa4.getX() + 1, fa4.getY());
        fa1.choque(p2, fa1.getX() + 1, fa1.getY());
        fa2.choque(p2, fa2.getX() + 1, fa2.getY());
        fa3.choque(p2, fa3.getX() + 1, fa3.getY());

        p3.setX(45);
        p3.setY(40);
        p3.setAncho(80);
        p3.setLargo(5);
        p3.pintarPared(g2d);
        p.choque(p3, p.getX(), p.getY() - 1);
        fa4.choque(p3, fa4.getX(), fa4.getY() - 1);
        fa1.choque(p3, fa1.getX(), fa1.getY() - 1);
        fa2.choque(p3, fa2.getX(), fa2.getY() - 1);
        fa3.choque(p3, fa3.getX(), fa3.getY() - 1);

        p4.setX(45);
        p4.setY(45);
        p4.setAncho(80);
        p4.setLargo(5);
        p4.pintarPared(g2d);
        p.choque(p4, p.getX(), p.getY() + 1);
        fa4.choque(p4, fa4.getX(), fa4.getY() + 1);
        fa1.choque(p4, fa1.getX(), fa1.getY() + 1);
        fa2.choque(p4, fa2.getX(), fa2.getY() + 1);
        fa3.choque(p4, fa3.getX(), fa3.getY() + 1);

        p5.setX(170);
        p5.setY(20);
        p5.setAncho(5);
        p5.setLargo(30);
        p5.pintarPared(g2d);
        p.choque(p5, p.getX() - 1, p.getY());
        fa4.choque(p5, fa4.getX() - 1, fa4.getY());
        fa1.choque(p5, fa1.getX() - 1, fa1.getY());
        fa2.choque(p5, fa2.getX() - 1, fa2.getY());
        fa3.choque(p5, fa3.getX() - 1, fa3.getY());

        p6.setX(175);
        p6.setY(20);
        p6.setAncho(5);
        p6.setLargo(30);
        p6.pintarPared(g2d);
        p.choque(p6, p.getX() + 1, p.getY());
        fa4.choque(p6, fa4.getX() + 1, fa4.getY());
        fa1.choque(p6, fa1.getX() + 1, fa1.getY());
        fa2.choque(p6, fa2.getX() + 1, fa2.getY());
        fa3.choque(p6, fa3.getX() + 1, fa3.getY());

        p5.setX(400);
        p5.setY(20);
        p5.setAncho(5);
        p5.setLargo(30);
        p5.pintarPared(g2d);
        p.choque(p5, p.getX() - 1, p.getY());
        fa4.choque(p5, fa4.getX() - 1, fa4.getY());
        fa1.choque(p5, fa1.getX() - 1, fa1.getY());
        fa2.choque(p5, fa2.getX() - 1, fa2.getY());
        fa3.choque(p5, fa3.getX() - 1, fa3.getY());

        p6.setX(405);
        p6.setY(20);
        p6.setAncho(5);
        p6.setLargo(30);
        p6.pintarPared(g2d);
        p.choque(p6, p.getX() + 1, p.getY());
        fa4.choque(p6, fa4.getX() + 1, fa4.getY());
        fa1.choque(p6, fa1.getX() + 1, fa1.getY());
        fa2.choque(p6, fa2.getX() + 1, fa2.getY());
        fa3.choque(p6, fa3.getX() + 1, fa3.getY());

        p7.setX(130);
        p7.setY(90);
        p7.setAncho(48);
        p7.setLargo(5);
        p7.pintarPared(g2d);
        p.choque(p7, p.getX(), p.getY() - 1);
        fa4.choque(p7, fa4.getX(), fa4.getY() - 1);
        fa1.choque(p7, fa1.getX(), fa1.getY() - 1);
        fa2.choque(p7, fa2.getX(), fa2.getY() - 1);
        fa3.choque(p7, fa3.getX(), fa3.getY() - 1);

        p8.setX(130);
        p8.setY(95);
        p8.setAncho(48);
        p8.setLargo(5);
        p8.pintarPared(g2d);
        p.choque(p8, p.getX(), p.getY() + 1);
        fa4.choque(p8, fa4.getX(), fa4.getY() + 1);
        fa1.choque(p8, fa1.getX(), fa1.getY() + 1);
        fa2.choque(p8, fa2.getX(), fa2.getY() + 1);
        fa3.choque(p8, fa3.getX(), fa3.getY() + 1);

        p9.setX(400);
        p9.setY(90);
        p9.setAncho(48);
        p9.setLargo(5);
        p9.pintarPared(g2d);
        p.choque(p9, p.getX(), p.getY() - 1);
        fa4.choque(p9, fa4.getX(), fa4.getY() - 1);
        fa1.choque(p9, fa1.getX(), fa1.getY() - 1);
        fa2.choque(p9, fa2.getX(), fa2.getY() - 1);
        fa3.choque(p9, fa3.getX(), fa3.getY() - 1);

        p10.setX(400);
        p10.setY(95);
        p10.setAncho(48);
        p10.setLargo(5);
        p10.pintarPared(g2d);
        p.choque(p10, p.getX(), p.getY() + 1);
        fa4.choque(p10, fa4.getX(), fa4.getY() + 1);
        fa1.choque(p10, fa1.getX(), fa1.getY() + 1);
        fa2.choque(p10, fa2.getX(), fa2.getY() + 1);
        fa3.choque(p10, fa3.getX(), fa3.getY() + 1);

        p11.setX(220);
        p11.setY(40);
        p11.setAncho(140);
        p11.setLargo(5);
        p11.pintarPared(g2d);
        p.choque(p11, p.getX(), p.getY() - 1);
        fa4.choque(p11, fa4.getX(), fa4.getY() - 1);
        fa1.choque(p11, fa1.getX(), fa1.getY() - 1);
        fa2.choque(p11, fa2.getX(), fa2.getY() - 1);
        fa3.choque(p11, fa3.getX(), fa3.getY() - 1);

        p12.setX(220);
        p12.setY(45);
        p12.setAncho(140);
        p12.setLargo(5);
        p12.pintarPared(g2d);
        p.choque(p12, p.getX(), p.getY() + 1);
        fa4.choque(p12, fa4.getX(), fa4.getY() + 1);
        fa1.choque(p12, fa1.getX(), fa1.getY() + 1);
        fa2.choque(p12, fa2.getX(), fa2.getY() + 1);
        fa3.choque(p12, fa3.getX(), fa3.getY() + 1);

        p13.setX(460);
        p13.setY(40);
        p13.setAncho(80);
        p13.setLargo(5);
        p13.pintarPared(g2d);
        p.choque(p13, p.getX(), p.getY() - 1);
        fa4.choque(p13, fa4.getX(), fa4.getY() - 1);
        fa1.choque(p13, fa1.getX(), fa1.getY() - 1);
        fa2.choque(p13, fa2.getX(), fa2.getY() - 1);
        fa3.choque(p13, fa3.getX(), fa3.getY() - 1);

        p14.setX(460);
        p14.setY(45);
        p14.setAncho(80);
        p14.setLargo(5);
        p14.pintarPared(g2d);
        p.choque(p14, p.getX(), p.getY() + 1);
        fa4.choque(p14, fa4.getX(), fa4.getY() + 1);
        fa1.choque(p14, fa1.getX(), fa1.getY() + 1);
        fa2.choque(p14, fa2.getX(), fa2.getY() + 1);
        fa3.choque(p14, fa3.getX(), fa3.getY() + 1);

        p15.setX(530);
        p15.setY(45);
        p15.setAncho(5);
        p15.setLargo(100);
        p15.pintarPared(g2d);
        p.choque(p15, p.getX() - 1, p.getY());
        fa4.choque(p15, fa4.getX() - 1, fa4.getY());
        fa1.choque(p15, fa1.getX() - 1, fa1.getY());
        fa2.choque(p15, fa2.getX() - 1, fa2.getY());
        fa3.choque(p15, fa3.getX() - 1, fa3.getY());

        p16.setX(535);
        p16.setY(45);
        p16.setAncho(5);
        p16.setLargo(100);
        p16.pintarPared(g2d);
        p.choque(p16, p.getX() + 1, p.getY());
        fa4.choque(p16, fa4.getX() + 1, fa4.getY());
        fa1.choque(p16, fa1.getX() + 1, fa1.getY());
        fa2.choque(p16, fa2.getX() + 1, fa2.getY());
        fa3.choque(p16, fa3.getX() + 1, fa3.getY());

        p13.setX(50);
        p13.setY(320);
        p13.setAncho(80);
        p13.setLargo(5);
        p13.pintarPared(g2d);
        p.choque(p13, p.getX(), p.getY() - 1);
        fa4.choque(p13, fa4.getX(), fa4.getY() - 1);
        fa1.choque(p13, fa1.getX(), fa1.getY() - 1);
        fa2.choque(p13, fa2.getX(), fa2.getY() - 1);
        fa3.choque(p13, fa3.getX(), fa3.getY() - 1);

        p14.setX(50);
        p14.setY(325);
        p14.setAncho(80);
        p14.setLargo(5);
        p14.pintarPared(g2d);
        p.choque(p14, p.getX(), p.getY() + 1);
        fa4.choque(p14, fa4.getX(), fa4.getY() + 1);
        fa1.choque(p14, fa1.getX(), fa1.getY() + 1);
        fa2.choque(p14, fa2.getX(), fa2.getY() + 1);
        fa3.choque(p14, fa3.getX(), fa3.getY() + 1);

        p15.setX(50);
        p15.setY(225);
        p15.setAncho(5);
        p15.setLargo(105);
        p15.pintarPared(g2d);
        p.choque(p15, p.getX() + 1, p.getY());
        fa4.choque(p15, fa4.getX() + 1, fa4.getY());
        fa1.choque(p15, fa1.getX() + 1, fa1.getY());
        fa2.choque(p15, fa2.getX() + 1, fa2.getY());
        fa3.choque(p15, fa3.getX() + 1, fa3.getY());

        p16.setX(45);
        p16.setY(225);
        p16.setAncho(5);
        p16.setLargo(105);
        p16.pintarPared(g2d);
        p.choque(p16, p.getX() - 1, p.getY());
        fa4.choque(p16, fa4.getX() - 1, fa4.getY());
        fa1.choque(p16, fa1.getX() - 1, fa1.getY());
        fa2.choque(p16, fa2.getX() - 1, fa2.getY());
        fa3.choque(p16, fa3.getX() - 1, fa3.getY());

        p13.setX(460);
        p13.setY(320);
        p13.setAncho(80);
        p13.setLargo(5);
        p13.pintarPared(g2d);
        p.choque(p13, p.getX(), p.getY() - 1);
        fa4.choque(p13, fa4.getX(), fa4.getY() - 1);
        fa1.choque(p13, fa1.getX(), fa1.getY() - 1);
        fa2.choque(p13, fa2.getX(), fa2.getY() - 1);
        fa3.choque(p13, fa3.getX(), fa3.getY() - 1);

        p14.setX(460);
        p14.setY(325);
        p14.setAncho(80);
        p14.setLargo(5);
        p14.pintarPared(g2d);
        p.choque(p14, p.getX(), p.getY() + 1);
        fa4.choque(p14, fa4.getX(), fa4.getY() + 1);
        fa1.choque(p14, fa1.getX(), fa1.getY() + 1);
        fa2.choque(p14, fa2.getX(), fa2.getY() + 1);
        fa3.choque(p14, fa3.getX(), fa3.getY() + 1);

        p15.setX(530);
        p15.setY(225);
        p15.setAncho(5);
        p15.setLargo(105);
        p15.pintarPared(g2d);
        p.choque(p15, p.getX() - 1, p.getY());
        fa4.choque(p15, fa4.getX() - 1, fa4.getY());
        fa1.choque(p15, fa1.getX() - 1, fa1.getY());
        fa2.choque(p15, fa2.getX() - 1, fa2.getY());
        fa3.choque(p15, fa3.getX() - 1, fa3.getY());

        p16.setX(535);
        p16.setY(225);
        p16.setAncho(5);
        p16.setLargo(105);
        p16.pintarPared(g2d);
        p.choque(p16, p.getX() + 1, p.getY());
        fa4.choque(p16, fa4.getX() + 1, fa4.getY());
        fa1.choque(p16, fa1.getX() + 1, fa1.getY());
        fa2.choque(p16, fa2.getX() + 1, fa2.getY());
        fa3.choque(p16, fa3.getX() + 1, fa3.getY());

        p17.setX(170);
        p17.setY(320);
        p17.setAncho(5);
        p17.setLargo(30);
        p17.pintarPared(g2d);
        p.choque(p17, p.getX() - 1, p.getY());
        fa4.choque(p17, fa4.getX() - 1, fa4.getY());
        fa1.choque(p17, fa1.getX() - 1, fa1.getY());
        fa2.choque(p17, fa2.getX() - 1, fa2.getY());
        fa3.choque(p17, fa3.getX() - 1, fa3.getY());

        p18.setX(175);
        p18.setY(320);
        p18.setAncho(5);
        p18.setLargo(30);
        p18.pintarPared(g2d);
        p.choque(p18, p.getX() + 1, p.getY());
        fa4.choque(p18, fa4.getX() + 1, fa4.getY());
        fa1.choque(p18, fa1.getX() + 1, fa1.getY());
        fa2.choque(p18, fa2.getX() + 1, fa2.getY());
        fa3.choque(p18, fa3.getX() + 1, fa3.getY());

        p17.setX(405);
        p17.setY(320);
        p17.setAncho(5);
        p17.setLargo(30);
        p17.pintarPared(g2d);
        p.choque(p17, p.getX() + 1, p.getY());
        fa4.choque(p17, fa4.getX() + 1, fa4.getY());
        fa1.choque(p17, fa1.getX() + 1, fa1.getY());
        fa2.choque(p17, fa2.getX() + 1, fa2.getY());
        fa3.choque(p17, fa3.getX() + 1, fa3.getY());

        p18.setX(400);
        p18.setY(320);
        p18.setAncho(5);
        p18.setLargo(30);
        p18.pintarPared(g2d);
        p.choque(p18, p.getX() - 1, p.getY());
        fa4.choque(p18, fa4.getX() - 1, fa4.getY());
        fa1.choque(p18, fa1.getX() - 1, fa1.getY());
        fa2.choque(p18, fa2.getX() - 1, fa2.getY());
        fa3.choque(p18, fa3.getX() - 1, fa3.getY());

        p9.setX(400);
        p9.setY(270);
        p9.setAncho(48);
        p9.setLargo(5);
        p9.pintarPared(g2d);
        p.choque(p9, p.getX(), p.getY() - 1);
        fa4.choque(p9, fa4.getX(), fa4.getY() - 1);
        fa1.choque(p9, fa1.getX(), fa1.getY() - 1);
        fa2.choque(p9, fa2.getX(), fa2.getY() - 1);
        fa3.choque(p9, fa3.getX(), fa3.getY() - 1);

        p10.setX(400);
        p10.setY(275);
        p10.setAncho(48);
        p10.setLargo(5);
        p10.pintarPared(g2d);
        p.choque(p10, p.getX(), p.getY() + 1);
        fa4.choque(p10, fa4.getX(), fa4.getY() + 1);
        fa1.choque(p10, fa1.getX(), fa1.getY() + 1);
        fa2.choque(p10, fa2.getX(), fa2.getY() + 1);
        fa3.choque(p10, fa3.getX(), fa3.getY() + 1);

        p9.setX(130);
        p9.setY(270);
        p9.setAncho(48);
        p9.setLargo(5);
        p9.pintarPared(g2d);
        p.choque(p9, p.getX(), p.getY() - 1);
        fa4.choque(p9, fa4.getX(), fa4.getY() - 1);
        fa1.choque(p9, fa1.getX(), fa1.getY() - 1);
        fa2.choque(p9, fa2.getX(), fa2.getY() - 1);
        fa3.choque(p9, fa3.getX(), fa3.getY() - 1);

        p10.setX(130);
        p10.setY(275);
        p10.setAncho(48);
        p10.setLargo(5);
        p10.pintarPared(g2d);
        p.choque(p10, p.getX(), p.getY() + 1);
        fa4.choque(p10, fa4.getX(), fa4.getY() + 1);
        fa1.choque(p10, fa1.getX(), fa1.getY() + 1);
        fa2.choque(p10, fa2.getX(), fa2.getY() + 1);
        fa3.choque(p10, fa3.getX(), fa3.getY() + 1);

        p19.setX(220);
        p19.setY(150);
        p19.setAncho(5);
        p19.setLargo(60);
        p19.pintarPared(g2d);
        p.choque(p19, p.getX() - 1, p.getY());
        fa4.choque(p19, fa4.getX() - 1, fa4.getY());
        fa1.choque(p19, fa1.getX() - 1, fa1.getY());
        fa2.choque(p19, fa2.getX() - 1, fa2.getY());
        fa3.choque(p19, fa3.getX() - 1, fa3.getY());

        p20.setX(225);
        p20.setY(150);
        p20.setAncho(5);
        p20.setLargo(60);
        p20.pintarPared(g2d);
        p.choque(p20, p.getX() + 1, p.getY());
        fa4.choque(p20, fa4.getX() + 1, fa4.getY());
        fa1.choque(p20, fa1.getX() + 1, fa1.getY());
        fa2.choque(p20, fa2.getX() + 1, fa2.getY());
        fa3.choque(p20, fa3.getX() + 1, fa3.getY());

        p19.setX(350);
        p19.setY(150);
        p19.setAncho(5);
        p19.setLargo(60);
        p19.pintarPared(g2d);
        p.choque(p19, p.getX() - 1, p.getY());
        fa4.choque(p19, fa4.getX() - 1, fa4.getY());
        fa1.choque(p19, fa1.getX() - 1, fa1.getY());
        fa2.choque(p19, fa2.getX() - 1, fa2.getY());
        fa3.choque(p19, fa3.getX() - 1, fa3.getY());

        p20.setX(355);
        p20.setY(150);
        p20.setAncho(5);
        p20.setLargo(60);
        p20.pintarPared(g2d);
        p.choque(p20, p.getX() + 1, p.getY());
        fa4.choque(p20, fa4.getX() + 1, fa4.getY());
        fa1.choque(p20, fa1.getX() + 1, fa1.getY());
        fa2.choque(p20, fa2.getX() + 1, fa2.getY());
        fa3.choque(p20, fa3.getX() + 1, fa3.getY());

        p21.setX(220);
        p21.setY(210);
        p21.setAncho(140);
        p21.setLargo(5);
        p21.pintarPared(g2d);
        fa1.choque(p21, fa1.getX(), fa1.getY() - 1);
        fa2.choque(p21, fa2.getX(), fa2.getY() - 1);
        fa3.choque(p21, fa3.getX(), fa3.getY() - 1);
        fa4.choque(p21, fa4.getX(), fa4.getY() - 1);

        p11.setX(220);
        p11.setY(320);
        p11.setAncho(140);
        p11.setLargo(5);
        p11.pintarPared(g2d);
        p.choque(p11, p.getX(), p.getY() - 1);
        fa1.choque(p11, fa1.getX(), fa1.getY() - 1);
        fa2.choque(p11, fa2.getX(), fa2.getY() - 1);
        fa3.choque(p11, fa3.getX(), fa3.getY() - 1);
        fa4.choque(p11, fa4.getX(), fa4.getY() - 1);

        p12.setX(220);
        p12.setY(325);
        p12.setAncho(140);
        p12.setLargo(5);
        p12.pintarPared(g2d);
        p.choque(p12, p.getX(), p.getY() + 1);
        fa1.choque(p12, fa1.getX(), fa1.getY() + 1);
        fa2.choque(p12, fa2.getX(), fa2.getY() + 1);
        fa3.choque(p12, fa3.getX(), fa3.getY() + 1);
        fa4.choque(p12, fa4.getX(), fa4.getY() + 1);

        p22.setX(220);
        p22.setY(215);
        p22.setAncho(140);
        p22.setLargo(5);
        p22.pintarPared(g2d);
        p.choque(p22, p.getX(), p.getY() + 1);
        fa1.choque(p22, fa1.getX(), fa1.getY() + 1);
        fa2.choque(p22, fa2.getX(), fa2.getY() + 1);
        fa3.choque(p22, fa3.getX(), fa3.getY() + 1);
        fa4.choque(p22, fa4.getX(), fa4.getY() + 1);

        p23.setColor(Color.BLACK);
        p23.setX(235);
        p23.setY(150);
        p23.setAncho(110);
        p23.setLargo(5);
        p23.pintarPared(g2d);
        p.choque(p23, p.getX(), p.getY() - 1);

        p.setColor(Color.yellow);
        p.pintar(g2d);

        fa1.pintar(g);

        fa2.pintar(g);

        fa3.pintar(g);

        fa4.pintar(g);
        fa1.matar(p, this);
        fa2.matar(p, this);
        fa3.matar(p, this);
        fa4.matar(p, this);

        while (pausa == true) {
            g2d.setColor(Color.GRAY);
            g2d.fillRect((getWidth() / 2) - 100, (getHeight() / 2) - 35, 200, 70);
            g2d.setColor(Color.BLUE);
            g2d.fillRect((getWidth() / 2) - 75, (getHeight() / 2) - 25, 150, 50);

            g2d.setColor(Color.YELLOW);
            g2d.drawString("PAUSA", (getWidth() / 2) - 30, (getHeight() / 2) + 10);
            break;
        }
        while (empezar == false) {
            g2d.setColor(Color.white);
            g2d.drawString("Presione S para empezar", (getWidth() / 2) - 120, (getHeight() / 2) - 70);
            break;
        }

        while (pausa == false && empezar == true) {
            p.mover();
            fa1.setNumero(random);
            fa2.setNumero(random2);
            fa3.setNumero(random3);
            fa4.setNumero(random4);
            fa4.moverF();
            fa1.moverF();
            fa2.moverF();
            fa3.moverF();
            break;
        }

    }

    @Override
    public void keyTyped(KeyEvent ke
    ) {
    }

    @Override
    public void keyPressed(KeyEvent ke
    ) {
        if (ke.getKeyCode() == KeyEvent.VK_S) {
            empezar = true;
        }

        if (ke.getKeyCode() == KeyEvent.VK_F) {
            cerrar = true;
        }

        if (ke.getKeyCode() == KeyEvent.VK_R) {
            resetTecla();
        }

        if (ke.getKeyCode() == KeyEvent.VK_P && pausa == false) {
            pausa = true;
        } else if (ke.getKeyCode() == KeyEvent.VK_P && pausa == true) {
            pausa = false;
        }

        random = (int) (Math.random() * 4) + 1;
        random2 = (int) (Math.random() * 4) + 1;
        random3 = (int) (Math.random() * 4) + 1;
        random4 = (int) (Math.random() * 4) + 1;

        p.mover(ke.getKeyCode());
        repaint();
        if (ke.getKeyCode() == KeyEvent.VK_H) {
            pausa = true;
            JOptionPane.showMessageDialog(null, "Puntuaciones\n" + list);

        }
    }

    @Override
    public void keyReleased(KeyEvent ke
    ) {
    }

}
