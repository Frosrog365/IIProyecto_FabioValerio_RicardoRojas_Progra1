/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PacUTN;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;

/**
 *
 * @author Fabio_2
 */
public class Pacman {

    int bolitasComidas;
    int con;
    private int x;
    private int y;
    private int diametro;
    private int speedX;
    private int speedY;
    private int score;
    int puntosvida;
    int puntosFruta;
    private Color color;
    private int vida;

    public int getVida() {
        return vida;
    }

    public void setVida(int vida) {
        this.vida = vida;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getSpeedX() {
        return speedX;
    }

    public void setSpeedX(int speedX) {
        this.speedX = speedX;
    }

    public int getSpeedY() {
        return speedY;
    }

    public void setSpeedY(int speedY) {
        this.speedY = speedY;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getDiametro() {
        return diametro;
    }

    public void setDiametro(int diametro) {
        this.diametro = diametro;
    }

    public Pacman() {
    }

    public void pintar(Graphics g) {
        g.setColor(color);
        g.fillOval(x, y, 20, 20);
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getCon() {
        return con;
    }

    public void setCon(int con) {
        this.con = con;
    }

    public void mover(int KeyCode) {
        switch (KeyCode) {
            case KeyEvent.VK_DOWN:
                speedX = 0;
                speedY = 1;
                y += 1;
                break;
            case KeyEvent.VK_UP:
                speedX = 0;
                speedY = -1;
                y -= 1;
                break;
            case KeyEvent.VK_RIGHT:
                speedY = 0;
                speedX = 1;
                x += 1;
                break;
            case KeyEvent.VK_LEFT:
                speedY = 0;
                speedX = -1;
                x -= 1;
                break;
        }
    }

    public void moverP(int random) {
        switch (random) {
            case 1:
                speedX = 0;
                speedY = 1;
                y += 1;
                break;
            case 2:
                speedX = 0;
                speedY = 1;
                y -= 1;
                break;
            case 3:
                speedY = 0;
                speedX = 1;
                x += 1;
                break;
            case 4:
                speedY = 0;
                speedX = 1;
                x -= 1;
                break;
        }
    }

    public Rectangle getBounds() {
        return new Rectangle(x, y, 20, 20);
    }

    public void choque(Pared pared, int PosX, int PosY) {
        if (getBounds().intersects(pared.getBounds())) {
            x = PosX;
            y = PosY;
            speedX = 0;
            speedY = 0;
        }
    }

    public void choqueFantasma(Pared pared, int random, int posX, int posY) {
        if (getBounds().intersects(pared.getBounds())) {
            speedX = 0;
            speedY = 0;
            x = posX;
            y = posY;
            if (x == posX) {
                moverP(1);
            } else if (y == posY) {
                moverP(2);
            }
        }

    }

    public void mover() {
        x += speedX;
        y += speedY;
    }

    /**
     * metodo que se utiliza para comer los puntos se va sumando diez en diez
     *
     * @param bolita ojeto colita que se toma de su Clase Bolita en la cual crea
     * un circulo en el cual el pacman va comiendo
     */
    public void comer(Bolita bolita, int con) {
        if (getBounds().intersects(bolita.getBounds())) {
            bolita.comido = true;
            score += 10;
            puntosvida += 10;
            puntosFruta += 10;
            con -= score;
            bolitasComidas++;
        }
    }

    /**
     * se habilita la fruta para comersela dando docientos puntos extras al
     * jugador y le da un tiempo de 100 a 400 puntos para poder cogerla si se
     * pasa no podra optener el plus de la fruta al pasar por la fruta
     * desaparece imediatamente dandole al jugador el plus de puntos primer
     * nivel 200 puntos por la cereza
     *
     * @param frut resibe la fruta el punto es comido para habilitarlo
     */
    public void comerFruta(Fruta frut, int con) {
        if (getBounds().intersects(frut.getBounds())) {
            frut.comido = true;
            score += 200;
            puntosvida += 200;
            puntosFruta += 200;
            
            con -= score;

        }

    }
    
    public void comerCerveza(Cerveza birra, int con) {
        if (getBounds().intersects(birra.getBounds())) {
            birra.comido = true;
            score += 200;
            puntosvida += 200;
            puntosFruta += 200;
            
            con -= score;

        }

    }

    /**
     *
     * @param bolitamax bolita para poder deborar al pacman
     * @param fa1 fantasmas que son deborados por el pacman
     * @param fa2 fantasmas que son deborados por el pacman
     * @param fa3 fantasmas que son deborados por el pacman
     * @param fa4 fantasmas que son deborados por el pacman
     */
    public void comerBolitaMax(BolitaMax bolitamax, Fantasmas fa1, Fantasmas fa2, Fantasmas fa3, Fantasmas fa4, int con) {
        if (getBounds().intersects(bolitamax.getBounds())) {
            bolitamax.comido = true;
            fa1.setComido(true);
            fa2.setComido(true);
            fa3.setComido(true);
            fa4.setComido(true);
            fa1.setColor(Color.BLUE);
            fa2.setColor(Color.BLUE);
            fa3.setColor(Color.BLUE);
            fa4.setColor(Color.BLUE);
            score += 50;
            puntosvida += 50;
            puntosFruta += 50;
            con -= score;
            bolitasComidas++;
        }
    }

    public void ganarVida() {
        if (puntosvida >= 2000) {
            vida++;
            puntosvida -= 2000;
        }
    }

}
